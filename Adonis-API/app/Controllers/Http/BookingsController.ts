import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'
export default class BookingsController {
     
    public async store({request, response}: HttpContextContract){
        /**
         * @swagger
         * /api/bookings:
         *   post:
         *     tags:
         *       - booking
         *     summary: Sample API
         *     parameters:
         *       - name: name
         *         description: Name of the user
         *         in: query
         *         required: false
         *         type: string
         *     responses:
         *       200:
         *         description: Send hello message
         *         example:
         *           message: Hello Guess
         */
        try {
            await request.validate( CreateBookingValidator )
            response.status(200).json(request.body())
            
        } catch (error) {
            response.badRequest(error.messages)
            
        }
    }
}
